﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SaveScore : MonoBehaviour
{
    private MovementSheep _movSheep;
    private MovementWolf _movWolf;
    public static int highScoreValue;
    public Text score;

    void Start()
    {
        score = GetComponent<Text>();
        _movSheep = FindObjectOfType<MovementSheep>();
        _movWolf = FindObjectOfType<MovementWolf>();
    }

    void Update()
    {
        score.text = " " + highScoreValue;
    }

}
