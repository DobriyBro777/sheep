﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountSheep : MonoBehaviour
{
    private NSScore _addScore;

    private void Start()
    {
        _addScore = FindObjectOfType<NSScore>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Delete")
        {
          Score.scoreValue += 1;
          Destroy(gameObject);
            if(LevelManager.nonStop == true)
            {
                _addScore.AddScore();
            }
        }
    }

    public void SheepHurt()
    {
        Destroy(this.gameObject);
    }

}
