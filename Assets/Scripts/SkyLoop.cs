﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyLoop : MonoBehaviour
{
    public float speed = 0.1f;
    private Vector2 offset = Vector2.zero;
    private Material _material;

    void Start()
    {
        _material = GetComponent<SpriteRenderer>().material;
        offset = _material.GetTextureOffset("_MainTex");
    }

    void Update()
    {
        offset.x += speed * Time.deltaTime;
        _material.SetTextureOffset("_MainTex", offset);
    }
}
