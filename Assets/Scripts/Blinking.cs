﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinking : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(c_Blinking(GetComponent<SpriteRenderer>()));
    }

    IEnumerator c_Blinking(SpriteRenderer image)
    {
        Color color = image.color;

        float alpha = 1.0f;

        while (true)
        {
            color.a = Mathf.MoveTowards(color.a, alpha, Time.deltaTime*0.4f);

            image.color = color;

            if (color.a == alpha)
            {
                if (alpha == 1.0f)
                {
                    alpha = 0.0f;
                }
            }

            yield return null;
        }
    }
}