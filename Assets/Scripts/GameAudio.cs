﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudio : MonoBehaviour {

    private AudioSource _soundGame;

    private void Start()
    {
        _soundGame = GetComponent<AudioSource>();
    }

    public void ActivateSoundGame()
    {
        _soundGame.enabled = true;
    }

    public void DeactivateSoundGame()
    {
        _soundGame.enabled = false;
    }
}
