﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

    private MovementSheep _movSheep;
    private MovementWolf _movWolf;
    public static int scoreValue = 0;
    public int  finishScore;
    public Text score;
    public static int nullScore;

 
    void Start ()
    {
        score = GetComponent<Text>();
        _movSheep = FindObjectOfType<MovementSheep>();
        _movWolf = FindObjectOfType<MovementWolf>();
    }

    void Update()
    {
        score.text = " " + scoreValue;
        if (scoreValue == finishScore)
        {
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(currentSceneIndex + 1);
            scoreValue = 0;
            CountHealth.healthPlayer = 3;
            Health.health = 3;
        }
    }
}
