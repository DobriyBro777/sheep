﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMovement : MonoBehaviour
{
    private Animator _anim;
    public float groundCheckRadius = 0.2f;
    public Transform groundCheckCollider;
    public LayerMask groudLayer;
    private bool _isGrounded;
    public bool pressedLeftButton = false;
    public bool pressedRightButton = false;
    public bool pressedJumpButton = false;

    public float horizontalspeed;
    float speedX;
    public float jumpForce = 6000;
    private Rigidbody2D _rigidBody;

    private float _jumpTimeCounter;
    public float jumpTime;
    private bool _isJumping;

    private bool _facingRight = true;

    void Start ()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }

  private void FixedUpdate ()
    {
        if(pressedLeftButton ==true)
        {
            OnLeftButtonDown();
        }
        if(pressedRightButton == true)
        {
            OnRightButtonDown();
        }
        if(Input.GetKey(KeyCode.A))
        {
            speedX = horizontalspeed;
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else if(Input.GetKey(KeyCode.D))
        {
            speedX = horizontalspeed;
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        transform.Translate(speedX, 0, 0);
        speedX = 0;
	}

   void Update()
    {
        _isGrounded = Physics2D.OverlapCircle(groundCheckCollider.position, groundCheckRadius, groudLayer);
        if (_isGrounded == true && Input.GetKeyDown(KeyCode.Space))
        {
            _isJumping = true;
            _jumpTimeCounter = jumpTime;
            _rigidBody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        }
        if (Input.GetKey(KeyCode.Space) && _isJumping == true)
        {
            if (_jumpTimeCounter > 0)
            {
                _rigidBody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
                _jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                _isJumping = false;
            }
        }
        if (Input.GetKey(KeyCode.Space))
        {
            _isJumping = false;
        }
    }

    public void OnLeftButtonDown()
    {
        pressedLeftButton = true;
        speedX = horizontalspeed;
        transform.rotation = Quaternion.Euler(0, 180, 0);
        
    }
 
    public void OnRightButtonDown()
    {
        pressedRightButton = true;
        speedX = horizontalspeed;
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void OnButtonUp()
    {
        pressedLeftButton = false;
        pressedRightButton = false;
        pressedJumpButton = false;
        speedX = 0f;
    }

    public void Jump()
    {
        pressedJumpButton = true;
        _isGrounded = Physics2D.OverlapCircle(groundCheckCollider.position, groundCheckRadius, groudLayer);
        if (_isGrounded == true && pressedJumpButton == true)
        {
            _isJumping = true;
            _jumpTimeCounter = jumpTime;
            _rigidBody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        }
        if (pressedJumpButton == true && _isJumping == true)
        {
            if (_jumpTimeCounter > 0)
            {
                _rigidBody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
                _jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                _isJumping = false;
            }
        }
        if (pressedJumpButton == true)
        {
            _isJumping = false;
        }
    }
}
