﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DestroyWolf : MonoBehaviour
{
    private Score _score;
    private EnemySpawner _spawner;
    private GameAudio _soundGame;

    private void Start()
    {
        _score = FindObjectOfType<Score>();
        _spawner = FindObjectOfType<EnemySpawner>();
        _soundGame = FindObjectOfType<GameAudio>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        CountSheep sheep = collision.collider.GetComponent<CountSheep>();
        if (sheep != null)
        {
            foreach (ContactPoint2D point in collision.contacts)
            {
                if (point.normal.y >= 0.5f)
                {
                    sheep.SheepHurt();
                }
            }
        }
        if (collision.gameObject.tag == "Delete")
        {
            Destroy(gameObject);
            Score.scoreValue = 0;
            NSScore.nsScoreValue = 0;
        }
    }
    public void EnemyHurt()
    {
        Destroy(this.gameObject);
    }
}

   

