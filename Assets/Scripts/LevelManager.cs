﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LevelManager : MonoBehaviour
{
    public static  bool nonStop = false;

    public void LoadNextScene()
    {
       nonStop = false;
       int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
       SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void BackToMenu()
    {
        nonStop = false;
        SceneManager.LoadScene("01MainMenu");
        Score.scoreValue = 0;
        CountHealth.healthPlayer = 3;
        Health.health = 3;
        MovementWolf.speed = 0.7f;
    }

    public void NonStop()
    {
        SceneManager.LoadScene("05LevelNonStop");
        nonStop = true;
        NSScore.nsScoreValue = 0;
        SaveScore.highScoreValue = 0;
        CountHealth.healthPlayer = 3;
        Health.health = 3;
        MovementWolf.speed = 0.7f;
    }

    public void LoadLastLevel()
    {
        nonStop = false;
        SceneManager.LoadScene("02Level1");
        Score.scoreValue = 0;
        CountHealth.healthPlayer = 3;
        Health.health = 3;
        MovementWolf.speed = 0.7f;
    }
    
    public void Quit()
    {
        Application.Quit();
    }
}
