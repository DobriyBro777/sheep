﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NSScore : MonoBehaviour
{
    private SaveScore _saveScore;
    private MovementSheep _movSheep;
    private MovementWolf _movWolf;
    public static int nsScoreValue;
    public Text score;

    public void Awake()
    {
        if(PlayerPrefs.HasKey("SaveScore"))
        {
            SaveScore.highScoreValue = PlayerPrefs.GetInt("SaveScore");
        }
    }

    void Start()
    {
        score = GetComponent<Text>();
        _movSheep = FindObjectOfType<MovementSheep>();
        _movWolf = FindObjectOfType<MovementWolf>();
        _saveScore = FindObjectOfType<SaveScore>();
    }

    void Update()
    {
        score.text = " " + nsScoreValue;
        if (nsScoreValue == 10)
        {
            MovementWolf.speed = MovementWolf.speed + 0.001f;
        }
        if (nsScoreValue == 20)
        {
            MovementWolf.speed = MovementWolf.speed + 0.002f;
        }
       
    }
    public void AddScore()
    {
        nsScoreValue++;

       if( nsScoreValue > SaveScore.highScoreValue)
        {
            SaveScore.highScoreValue = nsScoreValue;

            PlayerPrefs.SetInt("SaveScore", SaveScore.highScoreValue);
        }

    }
}   

