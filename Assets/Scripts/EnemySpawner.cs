﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private MovementSheep _sheep;
    [SerializeField] private MovementWolf _wolf;
    [SerializeField] private Transform _spawnPointSheep;
    [SerializeField] private Transform _spawnPointWolf;

    public float _minDelay;
    public float _maxDelay;
    private EnemySpawner spawner;

    private void Start()
    {
        StartCoroutine(SpawnSheep());
        StartCoroutine(SpawnWolf());
        spawner = FindObjectOfType<EnemySpawner>();
    }

    private IEnumerator SpawnSheep()
    {
        for(; ; )
        {
            var randomDelay = Random.Range(_minDelay, _maxDelay);
            yield return new WaitForSeconds(randomDelay);
            Instantiate(_sheep, _spawnPointSheep.position, Quaternion.identity);
        }
    }

    private IEnumerator SpawnWolf()
    {
        for (; ; )
        {
            var randomDelay = Random.Range(_minDelay, _maxDelay);
            yield return new WaitForSeconds(randomDelay);
            Instantiate(_wolf, _spawnPointWolf.position, Quaternion.identity);
        }
    }
}
