﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnim : MonoBehaviour {

    private Animator _anim;
    private CharacterMovement _charMov;

    void Start()
    {
        _anim = GetComponent<Animator>();
        _charMov = GetComponent<CharacterMovement>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || _charMov.pressedLeftButton || _charMov.pressedRightButton)
        {
            _anim.SetBool("isRunning", true);
        }
        else
        {
            _anim.SetBool("isRunning", false);
        }

        if(Input.GetKeyDown(KeyCode.Space) || _charMov.pressedJumpButton)
        {
            _anim.SetTrigger("jump");
        }
    }


}
