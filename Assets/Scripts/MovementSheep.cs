﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSheep : MonoBehaviour
{
    private Score score;
    public Vector2 direction;

    void Start()
    {
        transform.position = new Vector2(304, 126);
        score = FindObjectOfType<Score>();
    }

    void Update()
    {
        transform.Translate(direction * Time.deltaTime);
    }


}
