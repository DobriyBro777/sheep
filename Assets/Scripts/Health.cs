﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    private CountHealth _countHealth;
    public static int health = 3;
    private Material _matBlink;
    private Material _matDefault;
    private SpriteRenderer _spriteRend;

    private void Start()
    {
        _spriteRend = GetComponent<SpriteRenderer>();
        _matBlink = Resources.Load("PlayerBlink", typeof(Material)) as Material;
        _matDefault = _spriteRend.material;
        _countHealth = FindObjectOfType<CountHealth>();
    }

    void Hurt()
    {
        health--;
        _spriteRend.material = _matBlink;
        if (health <= 0 && LevelManager.nonStop == false)
        {
            SceneManager.LoadScene("04GameOver");
        }
        if (health <= 0 && LevelManager.nonStop == true)
        {
            SceneManager.LoadScene("06NSGameOver");
        }
        else
        {
            Invoke("ResetMaterial", 0.2f);
        }
    }

    void ResetMaterial()
    {
        _spriteRend.material = _matDefault;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        DestroyWolf enemy = collision.collider.GetComponent<DestroyWolf>();
        if (enemy != null)
        {
            foreach (ContactPoint2D point in collision.contacts)
            {
                if (point.normal.y >= 0.6f)
                {
                    enemy.EnemyHurt();
                }
                else
                {
                    Hurt();
                    _countHealth.MinusHealth();
                }

                break;
            }
        }
    }
}

