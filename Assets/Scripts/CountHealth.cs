﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountHealth : MonoBehaviour
{
    public static int healthPlayer = 3;
    public Text healthCounter;

    private void Start()
    {
        healthCounter = GetComponent<Text>();
    }

    private void Update()
    {
        healthCounter.text = " "+ healthPlayer;
    }

    public void MinusHealth()
    {
        healthPlayer--;
    }
}
